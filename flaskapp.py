import os
from datetime import datetime
from flask import Flask, request, flash, url_for, redirect, \
     render_template, abort, send_from_directory, session
import fetcher
from forms import EmailPasswordForm
import logging


app = Flask(__name__)
app.config.from_pyfile('flaskapp.cfg')

@app.route('/<path:resource>')
def serveStaticResource(resource):
    return send_from_directory('static/', resource)

@app.route("/test")
def test():
    return "<strong>It's Alive!</strong>"

if __name__ == '__main__':
    app.run(app.config['IP'], app.config['PORT'])

#posts, replies = fetcher.getPosts(515529705134198, ['prefeito', 'clayton', 'machado'])
#print posts
#print replies
#fetcher.getPosts(515529705134198, ['prefeito', 'clayton', 'machado', 'psdb', 'prefeitura'])

@app.route('/index')
@app.route('/')
def index():
    user = {'nickname': 'Vinicius'}  # fake user
    return render_template('index.html',
                            title = 'Candidatos',
                            user = user)


@app.route('/prefeito')
def prefeito():
    user = {'nickname': 'Cidadao'}  # fake user
    posts = fetcher.searchPostBatch(515529705134198, ['prefeito', 'clayton', 'machado', 'psdb', 'prefeitura'])
    return render_template('prefeito.html',
                            title = 'Prefeito Watch',
                            user = user,
                            posts = posts)

@app.route('/tonetti')
def tonetti():
    user = {'nickname': 'Cidadao'}  # fake user
    posts = fetcher.searchPostBatch(515529705134198, ['tonetti', 'alexandre', 'veronez', 'PTB', 'PT'])
    return render_template('prefeito.html',
                            title = 'Prefeito Watch',
                            user = user,
                            posts = posts)

@app.route('/tpmdb', methods=["GET", "POST"])
def login_pmdb():
    app.logger.warning('Page Loaded')
    form = EmailPasswordForm()
    #if 'username' in session:
        #return redirect(url_for('tpmdb'))

    if form.validate_on_submit():
        app.logger.warning('I was where')
        #<input id="email" name="email" type="text" value="t@t.com.br">
        app.logger.warning(form.email.data)
        app.logger.warning(form.password.data)
        #app.logger.warning(form.email.data)
        if form.email.data == "t@t.com.br":
            if form.password.data == "123":
                session['usuario'] = form.email.data
                session['partido'] = 'pmdb'
                return redirect(url_for('.tpmdb_pedefigo'))
            else:
                app.logger.warning('Senha incorreta')
                return redirect(url_for('.login_pmdb'))
        else:
            app.logger.warning('Email incorreto')
            app.logger.warning(form.email)
            return redirect(url_for('.login_pmdb'))

    #else:
        #app.logger.warning('RELOADING')
        #return redirect(url_for('.login_pmdb'))
    return render_template('login.html', form=form)

@app.route('/tpmdb/pedefigo', methods=["GET", "POST"])
def tpmdb_pedefigo():
    app.logger.warning("Para bailar la bamba")
    try:
        userLoggedIn = session['usuario']
    except Exception:
        return redirect(url_for('index'))
    partido = session['partido']
    app.logger.warning("EMAIL E PARTIDO")
    app.logger.warning(userLoggedIn)
    app.logger.warning(partido)
    if userLoggedIn:
        if partido == 'pmdb':
            user = {'nickname': userLoggedIn}
            posts = fetcher.searchPostBatch(515529705134198, ['orestes', 'previtale', 'giba', 'pmdb'])
            return render_template('prefeito.html',
                                    title = 'Prefeito Watch',
                                    user = user,
                                    posts = posts)
        else:
            return redirect(url_for('.login_pmdb'))
    else:
        return redirect(url_for('.login_pmdb'))
    return "OK"

@app.route('/tpmdb/somosvalinhos', methods=["GET", "POST"])
def tpmdb_somosvalinhos():
    try:
        userLoggedIn = session['usuario']
    except Exception:
    return redirect(url_for('index'))    partido = session['partido']
    if userLoggedIn:
        if partido == 'pmdb':
            user = {'nickname': 'Cidadao'}  # fake user
            posts = fetcher.searchPostBatch(286084758170343, ['orestes', 'previtale', 'giba', 'pmdb'])
            return render_template('prefeito.html',
                                    title = 'Prefeito Watch',
                                    user = user,
                                    posts = posts)
        else:
            return redirect(url_for('.login_pmdb'))
    else:
        return redirect(url_for('.login_pmdb'))
    return "OK"


@app.route('/tpmdb/terceiravisao', methods=["GET", "POST"])
def tpmdb_terceiravisao():
    try:
        userLoggedIn = session['usuario']
    except Exception:
        return redirect(url_for('index'))
    partido = session['partido']
    if userLoggedIn:
        if partido == 'pmdb':
            user = {'nickname': 'Cidadao'}  # fake user
            posts = fetcher.searchPostBatch(1501913536769933, ['orestes', 'previtale', 'giba', 'pmdb'])
            return render_template('prefeito.html',
                                    title = 'Prefeito Watch',
                                    user = user,
                                    posts = posts)
        else:
            return redirect(url_for('.login_pmdb'))
    else:
        return redirect(url_for('.login_pmdb'))
    return "OK"
