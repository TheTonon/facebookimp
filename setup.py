from setuptools import setup

setup(name='candidatos',
      version='0.2',
      description='',
      author='Vinicius Tonon',
      author_email='viniciustonon@gmail.com',
      url='http://www.python.org/sigs/distutils-sig/',
     install_requires=['Flask>=0.10.1'],
     )
