#!/usr/bin/env python2.7
import urllib2
import json
import time

appID = '621363411350875'
appSecret = '2bab5fc2924b99c1426b8d869cfb5fe9'
graphURL = 'https://graph.facebook.com/'

def getPostsv1(id, buscas):
    appID = globals()['appID']
    appSecret = globals()['appSecret']
    graphURL = globals()['graphURL']

    fetchurl = "https://graph.facebook.com/%(id)s/feed/?key=value&access_token=621363411350875|2bab5fc2924b99c1426b8d869cfb5fe9" % locals()
    openurl = urllib2.urlopen
    webResponse = openurl(fetchurl)
    page = webResponse.read()
    jsonPostData = json.loads(page)
    jsonFBPosts = jsonPostData['data']

    for post in jsonFBPosts:
        try:
            print post
            message = post['message']
            message = message.encode('utf8')
            postID = post['id']
            commenturl = "%(graphURL)s%(postID)s/comments/?key=value&access_token=%(appID)s|%(appSecret)s" % locals()

            webRespons = urllib2.urlopen(commenturl)
            cpage = webRespons.read()
            jsonCommentData = json.loads(cpage)
            jsonComments = jsonCommentData['data']


            for comment in jsonComments:
                cmessage = comment['message']
                cmessage = cmessage.encode('utf8')
                for item in buscas:
                    if item in cmessage.lower():
                        replies.append(comment)
                        posts.append(post)
            for item in buscas:
                print buscas
                if item in message.lower():
                    posts.append(post)
        except Exception:
            print 'Error'
    #posts = set(posts)
    #replies = set(replies)
    return posts, replies

def getCommentsForPost(postID):
    appID = globals()['appID']
    appSecret = globals()['appSecret']
    graphURL = globals()['graphURL']
    commenturl = "%(graphURL)s%(postID)s/comments/?key=value&access_token=%(appID)s|%(appSecret)s" % locals()
    #replies = []

    webResponse = urllib2.urlopen(commenturl)
    page = webResponse.read()
    jsonPostData = json.loads(page)
    jsonComments = jsonPostData['data']

    return jsonComments

def getRepliesToComment(commentID):
    appID = globals()['appID']
    appSecret = globals()['appSecret']
    graphURL = globals()['graphURL']
    commenturl = "%(graphURL)s%(commentID)s/comments/?key=value&access_token=%(appID)s|%(appSecret)s" % locals()

    webResponse = urllib2.urlopen(commenturl)
    page = webResponse.read()
    jsonPostData = json.loads(page)
    jsonReplies = jsonPostData['data']

    for post in jsonReplies:
        linecount = 0
        print '------>' + post['created_time']
        user = post['from']
        print '------>' + user['name']
        message = post['message']
        message = message.splitlines()
        for line in message:
            if linecount == 0:
                print '------>' + line.encode('utf8')
                linecount = linecount + 1
            else:
                print '       ' + line.encode('utf8')

def getPosts(groupid):
    print 'Im the Get Post, nice to meet you, sir.'
    appID = globals()['appID']
    appSecret = globals()['appSecret']
    graphURL = globals()['graphURL']

    fetchurl = "https://graph.facebook.com/%(groupid)s/feed/?key=value&access_token=621363411350875|2bab5fc2924b99c1426b8d869cfb5fe9" % locals()
    openurl = urllib2.urlopen
    webResponse = openurl(fetchurl)
    page = webResponse.read()
    jsonPostData = json.loads(page)
    jsonFBPosts = jsonPostData['data']

    posts = []

    for item in jsonFBPosts:

        nPost = post()
        aReplies = nPost.replies
        try:
            st = item['story']
            nPost.story = st.encode('utf8')
        except Exception as r:
            nPost.story = " "
            print r
        try:
            message = item['message']
            nPost.message = message.encode('utf8')
        except Exception as f:
            nPost.message = " "
        try:
            nPost.timestamp = item['updated_time']
            replies = getCommentsForPost(item['id'])
            for comment in replies:
                nReply = reply()
                user = comment['from']
                user = user['name']
                nReply.user = user.encode('utf8')
                nReply.timestamp = comment['created_time']
                comm = comment['message']
                comm = comm.encode('utf8')
                nReply.message = comm
                aReplies.append(nReply)
        except Exception as e:
            print e
        nPost.replies = aReplies
        posts.append(nPost)
    return posts

def searchPostBatch(groupid, buscas):
    print 'Im the Search Post Batch'
    posts = getPosts(groupid)
    nPosts = []
    for post in posts:
        for busca in buscas:
            if busca in post.message:
                nPosts.append(post)
                break
            for comment in post.replies:
                if busca in comment.message:
                    nPosts.append(post)
                    break
    nPosts = list(set(nPosts))
    return nPosts

class post:
    def __init__(self):
        self.story = ''
        self.timestamp = ''
        self.message = ''
        self.replies = []

class reply:
    def __init__(self):
        self.user = ''
        self.timestamp = ''
        self.message = ''

#getPosts(515529705134198, ['prefeito', 'clayton', 'machado', 'psdb', 'prefeitura'])
